/* Compile with
 * gcc -g -o testextents tests/testextents.c `pkg-config --cflags --libs pangocairo`
 */

#include <pango/pangocairo.h>

static void
get_hb_glyph_ink_extents (hb_font_t      *hb_font,
                          PangoGravity    gravity,
                          PangoGlyph      glyph,
                          PangoRectangle *ink)
{
  hb_glyph_extents_t extents;

  hb_font_get_glyph_extents (hb_font, glyph, &extents);

  /* These gymnastics are to make this function return
   * the same extents that the cairo font implementation
   * did.
   */
  switch (gravity)
    {
    case PANGO_GRAVITY_SOUTH:
      ink->x = extents.x_bearing;
      ink->y = - extents.y_bearing;
      ink->width = extents.width;
      ink->height = - extents.height;
      break;
    case PANGO_GRAVITY_EAST:
      ink->x = - extents.y_bearing;
      ink->y = - extents.x_bearing - extents.width;
      ink->width = - extents.height;
      ink->height = extents.width;
      break;
    case PANGO_GRAVITY_NORTH:
      ink->x = extents.x_bearing + extents.width;
      ink->y = - extents.y_bearing - extents.height;
      ink->width = - extents.width;
      ink->height = extents.height;
      break;
    case PANGO_GRAVITY_WEST:
      ink->x = - extents.y_bearing - extents.height;
      ink->y = - extents.x_bearing;
      ink->width = extents.height;
      ink->height = - extents.width;
      break;
    case PANGO_GRAVITY_AUTO:
    default:
      g_assert_not_reached ();
    }
}

static void
get_hb_glyph_logical_extents (hb_font_t      *hb_font,
                              PangoGravity    gravity,
                              PangoGlyph      glyph,
                              PangoRectangle *log)
{
  hb_font_extents_t font_extents;
  hb_position_t h_advance;

  hb_font_get_h_extents (hb_font, &font_extents);
  h_advance = hb_font_get_glyph_h_advance (hb_font, glyph);

  switch (gravity)
    {
    case PANGO_GRAVITY_SOUTH:
      log->x = 0;
      log->y = - font_extents.ascender;
      log->width = h_advance;
      log->height = font_extents.ascender - font_extents.descender;
      break;
    case PANGO_GRAVITY_EAST:
      log->x = - font_extents.ascender;
      log->y = - h_advance;
      log->width = font_extents.ascender - font_extents.descender;
      log->height = h_advance;
      break;
    case PANGO_GRAVITY_NORTH:
      log->x = 0;
      log->y = - font_extents.descender;
      log->width = h_advance;
      log->height = font_extents.descender - font_extents.ascender;
      break;
    case PANGO_GRAVITY_WEST:
      log->x = - font_extents.descender;
      log->y = - h_advance;
      log->width = font_extents.descender - font_extents.ascender;
      log->height = h_advance;
      break;
    case PANGO_GRAVITY_AUTO:
    default:
      g_assert_not_reached ();
    }
}

static void
get_logical_extents (PangoFont      *font,
                     PangoGlyph      glyph,
                     PangoRectangle *log)
{
  PangoFontDescription *desc;
  PangoGravity gravity;
  cairo_scaled_font_t *scaled_font;
  cairo_glyph_t cairo_glyph;
  cairo_text_extents_t extents;
  cairo_font_extents_t font_extents;

  desc = pango_font_describe (font);
  gravity = pango_font_description_get_gravity (desc);
  pango_font_description_free (desc);

  scaled_font = pango_cairo_font_get_scaled_font (PANGO_CAIRO_FONT (font));

  cairo_glyph.index = glyph;
  cairo_glyph.x = 0.;
  cairo_glyph.y = 0.;

  cairo_scaled_font_extents (scaled_font, &font_extents);
  cairo_scaled_font_glyph_extents (scaled_font, &cairo_glyph, 1, &extents);

  switch (gravity)
    {
    case PANGO_GRAVITY_SOUTH:
      log->x = 0;
      log->y = - pango_units_from_double (font_extents.ascent);
      log->width = pango_units_from_double (extents.x_advance);
      log->height = pango_units_from_double (font_extents.ascent + font_extents.descent);
      break;
    case PANGO_GRAVITY_EAST:
      log->x = - pango_units_from_double (font_extents.ascent);
      log->y = pango_units_from_double (extents.y_advance);
      log->width = pango_units_from_double (font_extents.ascent + font_extents.descent);
      log->height = - pango_units_from_double (extents.y_advance);
      break;

    case PANGO_GRAVITY_NORTH:
      log->x = 0;
      log->y = - pango_units_from_double (font_extents.descent);
      log->width = pango_units_from_double (extents.x_advance);
      log->height = pango_units_from_double (font_extents.ascent + font_extents.descent);
      break;
    case PANGO_GRAVITY_WEST:
      log->y = pango_units_from_double (extents.y_advance);
      log->x = - pango_units_from_double (font_extents.descent);
      log->height = - pango_units_from_double (extents.y_advance);
      log->width = pango_units_from_double (font_extents.ascent + font_extents.descent);
      break;
    case PANGO_GRAVITY_AUTO:
    default:
      g_assert_not_reached ();
    }
}

static void
draw_rectangle_with_basepoint (cairo_t *cr,
                               double   x,
                               double   y,
                               double   width,
                               double   height)
{
  cairo_rectangle (cr, x, y, width, height);
  cairo_stroke (cr);
  cairo_arc (cr, x, y, 4 * cairo_get_line_width (cr), 0, 2 * G_PI);
  cairo_close_path (cr);
  cairo_fill (cr);
}

static void
draw_with_gravity (PangoGravity gravity)
{
  PangoFontMap *fontmap;
  PangoContext *context;
  PangoFontDescription *desc;
  PangoFont *font;
  cairo_scaled_font_t *scaled_font;
  cairo_surface_t *surface;
  cairo_t *cr;
  cairo_glyph_t cairo_glyph;
  PangoRectangle ink, old_log, log;
  hb_font_t *hb_font;
  char *filename;
  gunichar ch;
  PangoGlyph glyph;

  fontmap = pango_cairo_font_map_get_default ();
  context = pango_font_map_create_context (fontmap);
  desc = pango_font_description_from_string ("Cantarell 64");
  pango_font_description_set_gravity (desc, gravity);
  font = pango_font_map_load_font (fontmap, context, desc);

  scaled_font = pango_cairo_font_get_scaled_font (PANGO_CAIRO_FONT (font));
  hb_font = pango_font_get_hb_font (font);

  ch = 'g';
  hb_font_get_nominal_glyph (hb_font, ch, &glyph);

  surface = cairo_image_surface_create (CAIRO_FORMAT_ARGB32, 400, 400);
  cr = cairo_create (surface);
  cairo_translate (cr, 200., 200.);

  cairo_set_source_rgb (cr, 1., 1., 1.);
  cairo_paint (cr);

  cairo_set_line_width (cr, 1.);
  cairo_set_source_rgb (cr, 0., 0., 0.);
  cairo_move_to (cr, -200., 0.5);
  cairo_line_to (cr, 200., 0.5);
  cairo_move_to (cr, 0.5, -200.);
  cairo_line_to (cr, 0.5, 200.);
  cairo_stroke (cr);

  cairo_move_to (cr, 0., 0.);
  cairo_set_scaled_font (cr, scaled_font);
  cairo_glyph.index = glyph;
  cairo_glyph.x = 0.;
  cairo_glyph.y = 0.;
  cairo_show_glyphs (cr, &cairo_glyph, 1);

  pango_font_get_glyph_extents (font, glyph, &ink, &old_log);
  get_logical_extents (font, glyph, &log);

  cairo_set_line_width (cr, 1.5);
  cairo_set_source_rgb (cr, 0., 0., 0.);
  draw_rectangle_with_basepoint (cr,
                                 ink.x / (double)PANGO_SCALE,
                                 ink.y / (double)PANGO_SCALE,
                                 ink.width / (double)PANGO_SCALE,
                                 ink.height / (double)PANGO_SCALE);

#if 0
  cairo_set_source_rgb (cr, 1., 0., 1.);
  draw_rectangle_with_basepoint (cr,
                                 old_log.x / (double)PANGO_SCALE,
                                 old_log.y / (double)PANGO_SCALE,
                                 old_log.width / (double)PANGO_SCALE,
                                 old_log.height / (double)PANGO_SCALE);
#endif

  cairo_set_source_rgb (cr, 1., 5., 0.);
  draw_rectangle_with_basepoint (cr,
                                 log.x / (double)PANGO_SCALE,
                                 log.y / (double)PANGO_SCALE,
                                 log.width / (double)PANGO_SCALE,
                                 log.height / (double)PANGO_SCALE);

  get_hb_glyph_ink_extents (hb_font, gravity, glyph, &ink);
  get_hb_glyph_logical_extents (hb_font, gravity, glyph, &log);

  cairo_set_source_rgb (cr, .8, .8, 0.);
#if 0
  draw_rectangle_with_basepoint (cr,
                                 ink.x / (double)PANGO_SCALE,
                                 ink.y / (double)PANGO_SCALE,
                                 ink.width / (double)PANGO_SCALE,
                                 ink.height / (double)PANGO_SCALE);
#endif

  draw_rectangle_with_basepoint (cr,
                                 log.x / (double)PANGO_SCALE,
                                 log.y / (double)PANGO_SCALE,
                                 log.width / (double)PANGO_SCALE,
                                 log.height / (double)PANGO_SCALE);

  filename = g_strdup_printf ("gravity%d.png", gravity);
  cairo_surface_write_to_png (surface, filename);
  g_free (filename);

  cairo_destroy (cr);
  cairo_surface_destroy (surface);
  g_object_unref (font);
  pango_font_description_free (desc);
  g_object_unref (context);
}

int
main (int argc, char *argv[])
{
  draw_with_gravity (PANGO_GRAVITY_SOUTH);
  draw_with_gravity (PANGO_GRAVITY_EAST);
  draw_with_gravity (PANGO_GRAVITY_NORTH);
  draw_with_gravity (PANGO_GRAVITY_WEST);

  return 0;
}
